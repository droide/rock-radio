<?php

function most_played( $date_from, $date_to ) {

	$file_content =  file( __DIR__ . '/data.txt' );

	// define arrays - if they aren't created IF (EMPTY()) returns error
	$dates_array = array();
	$titles_artists_array = array();
	$minutes_array = array();

	foreach( $file_content as $key => $value ) {
		// seperate date and title_artist in seperate arrays
		list( $minutes, $date, $title_artist ) = explode( "|", $value );

		// trim whitespaces
		$title_artist = rtrim( $title_artist );

		// create arrays just for songs played during this interval
		if( ( strtotime( $date ) >= strtotime( $date_from ) ) && ( strtotime( $date ) <= strtotime( $date_to ) ) ) {
			// remove errors and commercials from query
			if( $title_artist == "JSON NAPAKA - JSON NAPAKA" or
				$title_artist == "RADIO ROCK - V zivo" or 
				$title_artist == "RADIO ROCK - V živo" ) {
					unset( $key );
			}
			else {
				$dates_array[$key] = $date;
				$titles_artists_array[$key] = $title_artist;
			}
		}
	}

	if ( empty( $titles_artists_array ) ) {
		echo 'Ni podatkov za ta časovni razpon!';
		return array();
	}
	else {
		// count all occurrences of the songs: set keys of the array as songs (title + artist) and values as occurrences
		$ol_songs = array_count_values( $titles_artists_array );

		// remove empty key from array (new line in text file)
		unset( $ol_songs[null] );

		// sort array by values (occurrences)
		arsort( $ol_songs );
		return $ol_songs;
	}
}

?>