<?php

header('Content-Type: text/html; charset=UTF-8');
include 'functions.php';

?>

<html>
<head>
	<title>Rock radio - statistika predvajanja pesmi</title>

	<link rel="shortcut icon" href="http://testtao299.naravnedisave.si/logo.ico">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
	<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script type="text/javascript">
		// datepicker
		$(document).ready(function () {
			jQuery('.datepicker').datepicker({
			dateFormat: "dd.mm.yy"
			});
		});
	</script>

<!--END head-->
</head>

<body>

	<?php

	// define variables and set to empty values
	$date_from = $date_to = "";

	if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {
		$date_from = date( 'd.m.Y', strtotime( $_POST["date-from"] ) );
		$date_to = date( 'd.m.Y', strtotime( $_POST["date-to"] ) );
	}

	// set dates for most played songs for the current week
		// check the current day
		if( date( 'D' ) != 'Mon' ) {
			$date_from_week = date( 'd.m.Y', strtotime( 'last Monday' ) );
		}
		else {
			$date_from_week = date( 'd.m.Y' );
		}

		// take the next sunday
		if( date( 'D' ) != 'Sun' ) {
			$date_to_week = date( 'd.m.Y', strtotime( 'next Sunday' ) );
		}
		else {
			$date_to_week = date( 'd.m.Y' );
		}

	?>

	<h2>Največkrat predvajana skladba tega tedna (<?php echo $date_from_week . ' - ' . $date_to_week; ?>):</h2>

	<?php

	$songs_weekly = most_played( $date_from_week, $date_to_week );

	echo 'Št. predvajanj: ' . max( $songs_weekly ) . '<br>';

	foreach( $songs_weekly as $key => $value ) {
		if ( $value == max( $songs_weekly ) ) {
			echo ' - ' . $key . '<br>';
		}
	}

	?>

	<br>
	<h2>Časovni razpon statistike</h2>

	<form name="ajaxform" id="ajaxform" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" >
		<label for="date-from">
			Od
			<input type="text" id="date-from" class="datepicker" name="date-from" readonly />
		</label>

		<label for="date-to">
			Do
			<input type="text" id="date-to" class="datepicker" name="date-to" readonly />
		</label>

		<input type="submit" value="Izberi">
	</form>

	Prikazujem največkrat predvajane pesmi od <?php echo $date_from; ?> do (vključno) <?php echo $date_to; ?>:<br>

	<?php

	if( most_played( $date_from, $date_to ) != array() ) {
		foreach ( most_played( $date_from, $date_to ) as $key => $value ) {
			echo $value . ' - ' . $key . '<br>';
		}
	}

	?>

</body>

</html>