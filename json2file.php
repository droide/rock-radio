<?php

$url = 'http://rockradio.si/api/module/json/RadioSchedule/JsonModule/GetStreamInfo';
// initialize a new session and return a cURL handle for use with the curl_setopt(), curl_exec(), and curl_close() functions. 
$curl_resource = curl_init();

// set method to POST and set POST fields
curl_setopt( $curl_resource, CURLOPT_URL, $url );
curl_setopt( $curl_resource, CURLOPT_POST, true );
curl_setopt( $curl_resource, CURLOPT_POSTFIELDS, 'RadioStreamId=1&SystemName=&Title=&Description=&Icon%5BId%5D=0&Stream=&XmlStream=&Position=&Activated=true' );
curl_setopt( $curl_resource, CURLOPT_RETURNTRANSFER, true );

// grab URL and pass it to the browser
$result = curl_exec( $curl_resource );

// close cURL resource, and free up system resources
curl_close( $curl_resource );

// get the result and decode (true returns an array instead of an object)
$json_output = json_decode( $result, true );

if( empty( $json_output['data'] ) ) {
	$title = "JSON NAPAKA";
	$artist = "JSON NAPAKA";
	$created = date( 'd.m.Y' );
}
else {
	$title = $json_output['data'][0]['title'];
	$artist = $json_output['data'][0]['artist'];
	$created = date( 'd.m.Y' );
}

// filter (FRI*NO) from title and overwrite title
if ( preg_match( '/FRI.NO/', $title, $matches ) ) {
	$title = substr( $title, 0, -9 );
}

// create variable $current_song
$current_song = $title . " - " . $artist;

$file = 'data.txt';
$temp_file = 'current_song.txt';
$minutes_file = 'minutes_file.txt';

// if current_song.txt doesn't exist, create it
if ( ! file_exists( $temp_file ) ) {
	file_put_contents( __DIR__ . '/' . $temp_file, trim( $current_song ) );
}

// compare content in temporary text file and current song playing. If it is the same song, count minutes of playtime
// if it is not the same song, write it to data.txt file
if ( strcmp( trim( file_get_contents( __DIR__ . '/' . $temp_file ) ), trim( $current_song ) ) == 0 ) {
	// add one minute to minute counter
	$min = (int)trim( file_get_contents( __DIR__ . '/' . $minutes_file ) ) + 1;
	file_put_contents( __DIR__ . '/' . $minutes_file, $min );
}
else {
	// get number of minutes the song has played
	$min = (int)trim( file_get_contents( __DIR__ . '/' . $minutes_file ) );
	// reset minutes
	file_put_contents( __DIR__ . '/' . $minutes_file, '0' );

	// MIN|DD.MM.YYYY|TITLE - ARTIST
	$previous_song = $min . '|' . $created . '|' . trim( file_get_contents( __DIR__ . '/' . $temp_file ) ) . "\n";

	// write new song to data.txt and overwrite temp_file.txt with the current song
	file_put_contents( __DIR__ . '/' . $file, $previous_song, FILE_APPEND );
	file_put_contents( __DIR__ . '/' . $temp_file, $current_song );
}

?>